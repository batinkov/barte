#!/usr/bin/env python3

from io import StringIO
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage


class PdfDataExtractor:
    ''' A simple class that takes a .pdf file, processes it and return the content as a string. '''

    def __init__(self, file_name):
        # TODO: check if the file exists and is readable
        self.file_name = file_name
        self.__extracted_text = ''


    def __str__(self):
        # TODO: this is just a dummy implementation used for debugging
        return '### EXTRACTED TEXT BEGIN \n' + \
               self.__extracted_text + '\n' + \
               '### EXTRACTED TEXT END'


    def process(self):
        print('\t Extracting textual data.')

        output_string = StringIO()

        with open(self.file_name, 'rb') as f:
            parser = PDFParser(f)
            document = PDFDocument(parser)
            resource_manager = PDFResourceManager()
            device = TextConverter(resource_manager, output_string, laparams=LAParams())
            interpreter = PDFPageInterpreter(resource_manager, device)

            for page in PDFPage.create_pages(document):
                interpreter.process_page(page)

        self.__extracted_text = output_string.getvalue()


    def extracted_text(self):
        return self.__extracted_text

