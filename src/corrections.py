#!/usr/bin/env python3

import json


def apply_corrections(corrections_file_name, data_to_be_corrected):
    ''' Applies a list of predefined corrections to the extracted (from PdfDataExtractor) and processed (from QuestionParser) data. '''

    corrections = None

    with open(corrections_file_name) as f:
        corrections = json.load(f)

    for question in corrections['questions_to_be_corrected']:
        number = question['question_number']
        index = int(number) -1 # -1 because the indexing in python lists starts from 0 and the questions are indexed from 1
        assert(number == data_to_be_corrected[index]['question_number']) # make sure this is the correct question

        correction = question['corrections']

        for key in correction.keys():
            assert(key in data_to_be_corrected[index].keys()) # the key must already be present and it's just gonna be corrected here
            data_to_be_corrected[index][key] = correction[key]

