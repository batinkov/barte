#!/usr/bin/env python3

import re


class QuestionParser:
    def __init__(self, text, expected_pages, expected_questions):
        self.__text = text
        self.__expected_pages = expected_pages
        self.__expected_questions = expected_questions

        self.__page_numbers_pattern = re.compile(r'\d+\s\n\n\f')

        question_number = r'^(\d+)\.\s*'
        # The pattern question_body over complicated because there are typos in the pdf files. These are the rules:
            # 1 - Most questions end with "?" but there are some that end with "." and even "“".
        question_body = r'(.+?[?.“])'
        # The pattern correct_answer is over complicated because there are typos in the pdf files. Those are the rules:
            # 1 - In general all the questions end with "?" then "SPACE" then "(CORRECT_ANSWER_LETTER)".
            # 2 - The CORRECT_ANSWER_LETTER most of the time is Cyrillic А, Б, В, and Г, but sometimes it's also the Latin A or B.
            # 3 - Most of the time there is a space between the "?" and the correct answer but sometimes there isn't.
        correct_answer = r'\s*\(([АБВГAB])\)'
        self.__question_pattern = re.compile(''.join([question_number,
                                                      question_body,
                                                      correct_answer,
                                                     ]), re.MULTILINE | re.DOTALL)

        # The self.__question_matches list is the backbone of the text parsing. It marks the beginning and the end of a questions by
        # matching the question number, the question body and the correct answer.
        self.__question_matches = None

        self.__answer_a_pattern = re.compile(r'[АA]\.\s*', re.MULTILINE) # match both Cyrillic 'А' and Latin 'A'
        self.__answer_b_pattern = re.compile(r'Б\.\s*', re.MULTILINE)
        self.__answer_v_pattern = re.compile(r'[ВB]\.\s*', re.MULTILINE) # match both Cyrillic 'В' and Latin 'B'
        self.__answer_g_pattern = re.compile(r'Г\.\s*', re.MULTILINE)

        # A typical question looks like this: "1. На колко мегахерца (MHz) са равни 144432 килохерца (kHz)? (Б)"
        # It consist of three parts:
        # 1 - A question number. In that case "1. "
        # 2 - A question body. In that case "На колко мегахерца (MHz) са равни 144432 килохерца (kHz)?"
        # 3 - A correct answer. In that case "(Б)"
        # All the question will be collected in the self.__questions list in forms of dictionary with three keys: "question_number", "question_body", and "correct_answer".
        # The answer sets will be also collected in the list self.__questions in form of dictionary with keys: "А", "Б", "В", and "Г".
        # A typical answer set look like this:
        # А. На 0,144432 MHz;
        # Б. На 144,432 MHz;
        # В. На 144432000 MHz;
        # Г. На 0,000144432 MHz.
        # Sometimes the text from the pdfs is not extracted correctly and sometimes the answers are in form of pictures and this is why empty strings '' are allowed.
        # The errors and the empty answer sets will be corrected in a later stage of the data processing.
        self.__questions = []


    def __remove_page_numbers(self):
        ''' Try to filter the page numbers because they are not needed and might cause some problems with the next regexs. Make sure you remove only page numbers. '''

        # find what text will be removed
        text_to_be_removed = re.findall(self.__page_numbers_pattern, self.__text)

        # generate the list of page numbers that needs to be removed based on the page number
        text_expected_to_be_removed = []
        for n in range(1, self.__expected_pages+1):
            text_expected_to_be_removed.append('{} \n\n\x0c'.format(n))

        # the two lists should match
        if text_expected_to_be_removed == text_to_be_removed:
            print('\t Page numbers - safe to be removed.')
        else:
            # TODO: better error message
            assert(False)

        # remove page numbers (it's safe after the checks above)
        self.__text = re.sub(self.__page_numbers_pattern, '', self.__text)


    def __correct_the_degree_symbol(self):
        ''' The degree symbol '°' is represented in the pdf files by superscript Cyrillic 'о' or superscript Latin 'o'.
            This is used in many places and it's not convenient to be processed at the correction stage. That is why it
            is being corrected here.
        '''

        # The processing is tricky because the symbol have to be replaced only when it's preceded by digit and that digit must be preserved.
        self.__text = re.sub(r'(\d)о', r'\1°', self.__text) # Cyrillic 'о'
        self.__text = re.sub(r'(\d)o', r'\1°', self.__text) # Latin 'o'


    def __check_for_non_printable_symbols(self):
        for ch in self.__text:
            if ch == '\n':
                continue

            if not ch.isprintable():
                # TODO: better error message
                assert(False)

        print('\t No non-printable symbols were found (except \\n).')


    def __extract_header(self):
        header = self.__text[:self.__question_matches[0].start()]
        header = header.split('\n')
        header = filter(lambda x: x, header) # filter the empty strings
        header = filter(lambda x: not x.isspace(), header)
        header = list(header)

        if len(header) == 5:
            # this is a special case for 'Section 2' both Classes 1 and 2 where it's safe to join elements 1, 2, and 3
            header = [header[0], header[1] + header[2] + header[3], header[4]]

        if len(header) != 3:
            # TODO: better error message
            assert(False)

        self.__header = {
            'class':   header[0].strip(),
            'section': header[1].strip(),
            'update':  header[2].strip(),
        }


    def __extract_questions_with_correct_answers(self):
        # extract the questions - question numbers, question bodies, correct answers
        for index in range(len(self.__question_matches)):
            question_number = int(self.__question_matches[index].groups()[0])

            question_body = self.__question_matches[index].groups()[1].strip()
            question_body = self.__simplify_text(question_body)

            correct_answer = self.__question_matches[index].groups()[2].strip()
            correct_answer = correct_answer.replace('A', 'А') # translate Latin A to Cyrillic А (coming from the original document)
            correct_answer = correct_answer.replace('B', 'В') # translate Latin B to Cyrillic В (coming from the original document)

            self.__questions.append({'question_number': question_number,
                                     'question_body':   question_body,
                                     'correct_answer': correct_answer,
                                    })


        # sanity check for the question numbers - check if they are consecutive numbers starting from 1 and ending at the expected question number
        expected = list(range(1, self.__expected_questions+1))
        extracted = [x['question_number'] for x in self.__questions]
        if expected == extracted:
            print('\t Question numbers match the expected values.')
        else:
            # TODO: better error message
            assert(False)


        # check if all questions have number, body and answer
        expected = sorted(['question_number', 'question_body', 'correct_answer'])
        for question in self.__questions:
            extracted = sorted(question.keys())
            if expected != extracted:
                # TODO: better error message
                assert(False)

        print('\t The questions and the correct answers processed successfully.')


    def __extract_possible_answers(self):
        # find the start and the end of all the answer sets
        start = [x.end() for x in self.__question_matches]
        end = [x.start() for x in self.__question_matches[1:]]
        end.append(-1)

        # extract and process all the answer sets
        assert(len(start) == len(end) == len(self.__questions))
        for index in range(len(start)):
            answer = self.__text[start[index]:end[index]]

            # split the answers so we get all А, Б, В, Г
            A = re.search(self.__answer_a_pattern, answer)
            B = re.search(self.__answer_b_pattern, answer)
            V = re.search(self.__answer_v_pattern, answer)
            G = re.search(self.__answer_g_pattern, answer)

            current_question = self.__questions[index]

            current_question['А'] = self.__simplify_text(answer[A.end():B.start()]) if A and B else ''
            current_question['Б'] = self.__simplify_text(answer[B.end():V.start()]) if B and V else ''
            current_question['В'] = self.__simplify_text(answer[V.end():G.start()]) if V and G else ''
            current_question['Г'] = self.__simplify_text(answer[G.end():-1]) if G else ''


        ## all the keys 'А', 'Б', 'В', and 'Г' for the answer sets have to be present
        expected_keys = ['А', 'Б', 'В', 'Г']
        for question in self.__questions:
            for key in expected_keys:
                if not key in question.keys():
                    assert(False)


    def __simplify_text(self, text):
        text = re.sub(r'\n+', ' ', text)
        text = re.sub(r'[ ]+', ' ', text)
        text = text.strip()

        return text


    def parse(self):
        self.__remove_page_numbers()

        self.__correct_the_degree_symbol()

        self.__check_for_non_printable_symbols()

        # after the page numbers are removed do the main regex search (all the future processing depends on it)
        self.__question_matches = list(re.finditer(self.__question_pattern, self.__text))

        self.__extract_header()

        self.__extract_questions_with_correct_answers()

        self.__extract_possible_answers()


    def get_header(self):
        return self.__header.copy()


    def get_questions(self):
        return self.__questions[:]

