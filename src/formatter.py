#!/usr/bin/env python3

import json


def to_string(questions, header=None):
    result = ''

    if header: # the header is expected to have 'class', 'section', and 'update'
        result += header['class'] + '\n'
        result += header['section'] + '\n'
        result += header['update'] + '\n'
        result += '\n\n'

    for index in range(len(questions)):
        # process the question
        question = questions[index]
        result += '{0[question_number]}. {0[question_body]} ({0[correct_answer]})\n'.format(question)

        # process the possible answers
        keys = ['А', 'Б', 'В', 'Г']
        result += '{}. {}\n'.format(keys[0], question[keys[0]])
        result += '{}. {}\n'.format(keys[1], question[keys[1]])
        result += '{}. {}\n'.format(keys[2], question[keys[2]])
        result += '{}. {}\n'.format(keys[3], question[keys[3]])

        result += '\n'

    return result


def to_json(questions, header):
    result = dict(header, **{'questions': questions})

    return json.dumps(result, indent=4, ensure_ascii=False)

