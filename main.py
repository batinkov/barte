#!/usr/bin/env python3

# TODO: the messages printed with \t should be replaced with a generic logger

import json

from src.pdf_data_extractor import PdfDataExtractor
from src.question_parser import QuestionParser
from src.corrections import apply_corrections
import src.formatter as formatter


if __name__ == '__main__':
    files = None
    with open('./config.json', 'r') as c:
        config = json.load(c)
        files = config['files']

    for current_file in files:
        print('Processing "{}" ...'.format(current_file['file_name']))

        extractor = PdfDataExtractor(current_file['file_name'])
        extractor.process()
        pdf_raw_text = extractor.extracted_text()

        question_parser = QuestionParser(pdf_raw_text, expected_pages=current_file['pages'], expected_questions=current_file['questions'])
        question_parser.parse()
        header = question_parser.get_header()
        questions = question_parser.get_questions()

        apply_corrections(current_file['corrections'], questions)

        with open(current_file['out_file_name'], 'w') as of:
            # of.write(formatter.to_string(questions, header)) # for debugging
            of.write(formatter.to_json(questions, header))

        print('\n\n')

